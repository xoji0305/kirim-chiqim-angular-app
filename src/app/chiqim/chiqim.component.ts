import { Component, OnInit } from '@angular/core';
import { Model } from '../models/Model';
import  '../kirim/kirim.component';

@Component({
  selector: 'app-chiqim',
  templateUrl: './chiqim.component.html',
  styleUrls: ['./chiqim.component.css']
})
export class ChiqimComponent implements OnInit {
  chiqimArray: Model[];
  arrChiqim;
  sumChiqim: number = 0;
  sumKirim: number = 0;
  textInput: string = '';
  sumInput: string = '';
  qolganSumma: number = 0;

  constructor() { }

  ngOnInit(): void {
    this.chiqimArray = JSON.parse(localStorage.getItem("chiqim")) || [];
    this.sumChiqim = JSON.parse(localStorage.getItem('sumChiqim')) || 0;
    this.sumKirim = JSON.parse(localStorage.getItem('sumKirim')) || 0;
    this.qolganSumma = JSON.parse(localStorage.getItem('qolganSumma')) || 0;
  }
   updateStorage = () => {
    localStorage.setItem("chiqim", JSON.stringify(this.chiqimArray));
    localStorage.setItem("sumChiqim", JSON.stringify(this.sumChiqim));
    localStorage.setItem("sumKirim", JSON.stringify(this.sumKirim));
    localStorage.setItem("qolganSumma", JSON.stringify(this.qolganSumma));
  };
  addChiqim() {
    this.chiqimArray.push({
      id: this.chiqimArray.length + 1,
      name: this.textInput,
      sum: this.sumInput,
      date: (("0" + new Date().getDate()).slice(-2) + "-" + ("0"+(new Date().getMonth()+1)).slice(-2) + "-" +
      new Date().getFullYear() + " " + ("0" + new Date().getHours()).slice(-2) + ":" + ("0" + new Date().getMinutes()).slice(-2))
    })

    this.sumChiqim += Number(this.sumInput);
    this.qolganSumma = this.sumKirim - this.sumChiqim;

    this.updateStorage();
    this.textInput = '';
    this.sumInput = '';
  }

  deleteChiqimItem(id: number) {

    for (let i = 0; i < this.chiqimArray.length; i++) {
      if (JSON.parse(this.chiqimArray[i].id) === id) {
        this.sumChiqim -= Number(this.chiqimArray[i].sum); 
      }
    }
    
    this.arrChiqim = this.chiqimArray.findIndex((item) => {
      return JSON.parse(item.id) === id
    });
    
    this.chiqimArray.splice(this.arrChiqim, 1);
    
    this.updateStorage();
  }

  clearChiqim() {
    localStorage.removeItem('sumChiqim');
    this.qolganSumma = 0;
    this.sumChiqim = 0;
    this.chiqimArray = [];
    this.updateStorage();
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { KirimComponent } from './kirim/kirim.component';
import { ChiqimComponent } from './chiqim/chiqim.component';
import { NavbarComponent } from './navbar/navbar.component';

const routes: Routes = [
  { path: "", component: KirimComponent },
  { path: "chiqimlar", component: ChiqimComponent }
]
@NgModule({
  declarations: [
    AppComponent,
    KirimComponent,
    ChiqimComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { Model } from '../models/Model';

@Component({
  selector: 'app-kirim',
  templateUrl: './kirim.component.html',
  styleUrls: ['./kirim.component.css']
})
export class KirimComponent implements OnInit {

  kirimArray: Model[];
  arrKirim;
  sumKirim: number = 0;
  textInput: string = '';
  sumInput: string = '';
  qolganSumma: number = 0;
  
  constructor() { }

  ngOnInit(): void {
    this.kirimArray = JSON.parse(localStorage.getItem('kirim')) || [];
    this.sumKirim = JSON.parse(localStorage.getItem('sumKirim')) || 0;
    this.qolganSumma = JSON.parse(localStorage.getItem('qolganSumma')) || 0;
  };

   updateStorage = () => {
    localStorage.setItem("kirim", JSON.stringify(this.kirimArray));
    localStorage.setItem("sumKirim", JSON.stringify(this.sumKirim));
    localStorage.setItem("qolganSumma", JSON.stringify(this.qolganSumma));
  };

  addKirim() {
    this.kirimArray.push({
      id: this.kirimArray.length + 1,
      name: this.textInput,
      sum: this.sumInput,
      date: (("0" + new Date().getDate()).slice(-2) + "-" + ("0"+(new Date().getMonth()+1)).slice(-2) + "-" +
      new Date().getFullYear() + " " + ("0" + new Date().getHours()).slice(-2) + ":" + ("0" + new Date().getMinutes()).slice(-2))
    });

    this.sumKirim += Number(this.sumInput);
    this.qolganSumma = this.qolganSumma + Number(this.sumInput);

    this.updateStorage();
    this.textInput = '';
    this.sumInput = '';

  }

  deleteKirimItem(id: number) {
    
    for (let i = 0; i < this.kirimArray.length; i++) {
      if (JSON.parse(this.kirimArray[i].id) === id) {
        this.sumKirim -= Number(this.kirimArray[i].sum); 
      }
    }
    
    this.arrKirim = this.kirimArray.findIndex((item) => {
      return JSON.parse(item.id) === id
    });

    this.kirimArray.splice(JSON.parse(this.arrKirim), 1);

    this.updateStorage();
  }

  clearKirim() {
    localStorage.removeItem('sumKirim');
    this.qolganSumma = 0;
    this.kirimArray = [];
    this.sumKirim = 0;
    this.updateStorage();
  }

}
